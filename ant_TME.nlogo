globals[nestx nesty nest_loc]

;;globals(tempo)
breed[ants ant]

breed[smart_ants smart_ant]

breed [nests nest] 
;;directed-link-breed [path-links path-link]
breed[locations location]

locations-own [
  food                 ;; amount of food on this patch (0, 1, or 2)
  nest?                ;; true on nest patches, false elsewhere
  food_locations?
]

smart_ants-own[
  optimal_path
  optimal_distance
  path_taken
  food_transported
  optimal_path_save
  bool_voyage
]

ants-own 
[food_transported
  path_taken
  distance_parcouru
  optimal_path
  optimal_distance
]

links-own[pheromones]

to setup
  clear-all
  
  set-default-shape locations "circle"
  setup-patches ;setup of the colony and food location
  
  set-default-shape ants "bug"
  create-ants ant_population
  [ set size 0.7      
    set color red
    setxy nestx nesty
    set path_taken []
    set path_taken lput one-of locations-here path_taken
    show path_taken 
    set distance_parcouru 0
    set optimal_path []
    set optimal_distance 0
  ] 
  
  set-default-shape smart_ants "bug"
  create-smart_ants smart_ant_population
  [ set size 3.0      
    set bool_voyage False
    set color blue + 3
    setxy nestx nesty
    set path_taken []
    set path_taken lput one-of locations-here path_taken
    show path_taken 
    set optimal_path []
    set optimal_distance 0
    
  ] 
  
  reset-ticks
end

to reset
  ask ants[die]
  
  ask smart_ants[die]
  
  set-default-shape ants "bug"
  create-smart_ants smart_ant_population
  [ set size 3.0      
    set color blue + 3
    set bool_voyage False
    setxy nestx nesty
    set path_taken []
    set path_taken lput one-of locations-here path_taken
    show path_taken 
    set optimal_path []
    set optimal_distance 0
    set optimal_path_save []
  ] 
  
  set-default-shape ants "bug"
  create-ants ant_population[ 
    set size 0.7       
    set color red
    set distance_parcouru 0
    setxy nestx nesty
    set path_taken []
    set path_taken lput one-of locations-here path_taken
    set food_transported 0
    show path_taken 
    
  ]
  
  ask locations[
    if nest? = True [set food 0]
    if food_locations? = True[
      set food food_capacity 
    ]
    set size 0.5
  ]
  ask links [set pheromones 0 
    set color 61]
  
  reset-ticks
end



to setup-patches
  
  
  create-locations total_locations
  [;;setxy random-xcor random-ycor
    let xloc random-xcor
    let yloc random-ycor
    let test_pos False
    
    ask other locations[set test_pos test_pos and (xloc = xcor and yloc = ycor)]
    while [test_pos = True]
      [
        set xloc random-xcor
        set yloc random-ycor
        ask other locations[set test_pos test_pos and (xloc = xcor and yloc = ycor)]
        print test_pos
      ]
    
    set food_locations? False
    setxy xloc yloc
    set color blue
    set size 0.5
    set nest? false
  ]
  
  ask one-of locations[
    set nest? True 
    set nestx xcor set nesty ycor 
    set size 5
    set color green
    set shape "pentagon"
  ]
  
  ask n-of food_location locations with [nest? = False]
    [
      set food_locations? True 
      set food food_capacity 
      set size 5
      set color yellow
      set shape "plant"
    ]
  
  ask locations[
    ;;let neighbor-nodes turtle-set [locations-here] of neighbors4
    ;;let neibhor locations min-one-of (other locations)[distance myself]
    create-links-with other min-n-of nmb_location_connection other locations [ distance myself ] 
      [set pheromones 0]
  ]
  
end

to go
  let test_fin False
  ask locations [ if nest? = True [ if food > (food_capacity * food_location) - 1[set test_fin True]]
  ]
  
  let max_pheromone 0
  ask links [set max_pheromone max list pheromones max_pheromone]
  if max_pheromone != 0[
    ask links [set color 60 + 9 * (pheromones / max_pheromone)]
  ]
  ;Show "start"
  if test_fin != True[
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    
    
    ask smart_ants [
      ;show "what"
      
      if smart_ant?[
        show "deb"
        show optimal_path
        
        if optimal_distance > 0 [
          ;move to food loc
          
          if (food_transported = 0) and (bool_voyage = False) [;if no food transported and not going back to the coloni 
            let bool_nest False
            ask locations-here [set bool_nest nest?]
            if bool_nest
            [set optimal_path but-first optimal_path]
            
            ;show "here"
            
            let zone_ou_aller first optimal_path
            ;show zone_ou_aller
            move-to zone_ou_aller
            set optimal_path but-first optimal_path
            
            let food_test False
            let x 0
            if length optimal_path = 0[
              
              ask locations-here[set food_test food_locations? set x food]
              if food_test [;if its the place comunicated
                let food_transported_tampon min list ant_food_capacity x
                ask locations-here[set food food - food_transported_tampon]
                set food_transported food_transported_tampon
                set bool_voyage True
                if food_transported = 0 [
                  set optimal_distance 0
                  ;show"now"
                ]; if the spot is empty we take it of of the memory
                
                
              ]
            ]
            
          ]
        ]
        if bool_voyage;go back to the colony
          [;show "ici"
            let zone_presente one-of locations-here
            
            ;set path_taken but-last path_taken
            let zone_ou_aller last path_taken
            set path_taken but-last path_taken
            ; show zone_ou_aller
            move-to zone_ou_aller
            let bool False
            let food_tampon food_transported
            
            ask one-of locations-here
            [
              if nest? = True [
                ;show "Nest"
                set Bool True
                set food food + food_tampon]
            ]
            if bool = True
            [ set food_transported 0
              ;set optimal_distance 0
              set path_taken optimal_path_save
              set optimal_path optimal_path_save
              set bool_voyage False
              ;set path_taken []
              show "reset"
              show optimal_path_save
              show optimal_path
              ;set path_taken lput one-of locations-here path_taken
            ]
            
          ]
        
        
        
        
      ]
    ]
    
    
    
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    
    ask links[set pheromones (1 - evaporate_rate) * pheromones]
    
    ask ants[
      let tampon_opt_distance optimal_distance
      let tampon_opt_path_taken optimal_path
      let bool_opt False
      
      ask other ants-here[
        if (optimal_distance < tampon_opt_distance) and (optimal_distance != 0) [
          set bool_opt True
          set tampon_opt_distance optimal_distance
          set tampon_opt_path_taken optimal_path
          ]]
      if bool_opt = True [
        set optimal_path tampon_opt_path_taken
        set optimal_distance tampon_opt_distance
        ]
      if tampon_opt_distance > 0[
        ask smart_ants-here[
          ;show "se passa"
          ;show tampon_opt_distance
          if (optimal_distance > tampon_opt_distance) or (optimal_distance = 0) [
            show "ant question smart_ant"
            show tampon_opt_distance
            show tampon_opt_path_taken
            set optimal_distance tampon_opt_distance
            set optimal_path_save  tampon_opt_path_taken
            ; set optimal_path tampon_opt_path_taken
            ; set path_taken tampon_opt_path_taken
          ]
        ]
      ]


      
      ;show "NEW ants"
      ifelse food_transported = 0 ; if we dont have food we look for food in the location
        [;;look_for_food
          let x 0
          let op False
          ask  locations-here [
            set op nest?
            ifelse nest? = True
            [
              set x 0
            ]
            [
              set x food
            ]
            
          ]
          if op[
            set optimal_distance 0
            set optimal_path []
          ]
          
          let food_transported_tampon 0
          ifelse x > 0 ; if there's food we take it
          [set food_transported_tampon min list ant_food_capacity x
            ask locations-here[set food food - food_transported_tampon]
            set food_transported food_transported_tampon
            
            set optimal_distance distance_parcouru
            set optimal_path path_taken
            ;;can go home
          ]
          [;else we look for an other location
            let tempo []
            let tempo_name []
            let number_of_path 0
            let i 0
            let taunu []
            let Pij []
            let distances []
            ask locations-here[
              ;show "hello"
              ;; let tempo list 0 0
              ask links with [ end1 = myself or end2 = myself] 
              [ ;we ask the pheromone value
                ;show "dsf"
                set number_of_path number_of_path + 1
                ;;set tempo list tempo pheromones
                ifelse end1 = myself
                [
                  set tempo_name lput end2 tempo_name
                ]
                [
                  set tempo_name lput end1 tempo_name
                ]
                set taunu lput 0 taunu
                set tempo lput pheromones tempo
                set Pij lput 0 Pij
                let truc 0
                let a end2
                ask end1[set truc distance a]
                set distances lput truc distances
              ]
            ]
            ;show "distances"
            ;show distances
            ;show tempo_name
            ;set tempo 0
            ;need to calculate tau to make decision
            ;show item 1 taunu 
            while [i != number_of_path]
            [
              let up item i tempo
              let dist item i distances
              ifelse up != 0 
              [
                set up (up * alpha) * ( (1 / dist) * beta )
              ]
              [set up (1 / dist) * beta]
              
              let test_path False
              ;show tempo_name
              ;show path_taken
              foreach path_taken [set test_path test_path or ( ? = item i tempo_name)]
              
              if test_path = True [
                set up 0
                ;show "ici"
              ]
              
              ;show "allo"
              set taunu replace-item i taunu up
              ;set taunu i pheromone*(1/distance)
              set i i + 1
              
            ]
            
            set i 0
            let sommeTij 0
            while [i != number_of_path]
            [
              let g item i taunu
              set sommeTij sommeTij + g
              set i i + 1
              
            ]
            
            ifelse sommeTij != 0[
              let pio random-float 1
              
              set i 0
              
              while [i != number_of_path]
              [
                let g item i taunu
                let o g / sommeTij
                set Pij replace-item i Pij o
                set i i + 1
                
              ]
              let tampon_bas 0
              let tampon_haut item 0 Pij
              
              set i 0
              ;show "PIJ"
              ;show Pij
              ;show "choix chemin"
              ;show pio
              while [pio < tampon_bas or pio > tampon_haut ]
              [
                ;show "bas"
                ;show tampon_bas
                ; show "haut"
                ;show tampon_haut
                ;show "i"
                ;show i
                set i i + 1
                ;show pio
                ;show "bas"
                ; show tampon_bas
                ; show "haut"
                ; show tampon_haut
                set tampon_bas tampon_haut
                set tampon_haut tampon_haut + item i Pij
                
                
              ]
              ;show "choix"
              ;show i
              ;show "SMELL"
              ;show i
              let dist item i distances
              set distance_parcouru distance_parcouru + dist
              
              move-to item i tempo_name
              set path_taken lput item i tempo_name path_taken
              ;show path_taken
            ]
            [
              ;show "retour"
              ;show Pij
              ; show tempo_name
              let zone_presente one-of locations-here
              let zone_ou_aller last path_taken
              move-to zone_ou_aller
              let bool False
              let food_tampon food_transported
              let om distance_parcouru
              ask one-of locations-here [
                
                ask links with [ (end1 = myself or end2 = myself) and (end1 = zone_presente or end2 = zone_presente)]
                [
                  ;set pheromones pheromones + (food_tampon / om )
                  ;show om
                  
                  
                ]
              ]
              ask one-of locations-here
              [
                if nest? = True [
                  ;show "Nest"
                  set Bool True
                  set food food + food_tampon]
              ]
              if bool = True
              [ set food_transported 0
                set path_taken []
                set path_taken lput one-of locations-here path_taken
                
                set distance_parcouru 0
              ]
              
            ]
            
          ]
          set food_transported food_transported_tampon
          
        ]
        [;;back_home scroll down memorie and let pheromone on location because we already have food
         ;show "have to MOVEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE"
         ;show path_taken
         ;show food_transported
          let zone_presente one-of locations-here
          let zone_ou_aller last path_taken
          move-to zone_ou_aller
          set path_taken but-last path_taken
          let bool False
          let food_tampon food_transported
          let om distance_parcouru
          ask one-of locations-here [
            
            ask links with [ (end1 = myself or end2 = myself) and (end1 = zone_presente or end2 = zone_presente)]
              [
                set pheromones pheromones + (food_tampon / om )
                ;show om
                
                
              ]
          ]
          ask locations-here
          [
            if nest? = True [
              ;show "Nest"
              set Bool True
              set food food + food_tampon]
          ]
          if bool = True
          [ 
            let tampon_opt_dist optimal_distance
            let tampon_opt_path optimal_path
            
            
            ask smart_ants-here[
              
              if (optimal_distance > tampon_opt_dist) or (optimal_distance = 0)
              [
                set optimal_distance tampon_opt_dist 
                set optimal_path tampon_opt_path
                set path_taken tampon_opt_path
                set optimal_path_save tampon_opt_path
              ]
            ]
           
            set food_transported 0
            set path_taken []
            set path_taken lput one-of locations-here path_taken
            
            set distance_parcouru 0
          ]
          
          
          
        ]
    ]
    ask locations[ let compteur count ants-here
      
      set size 1 + (compteur / ant_population) * 5
      if food_locations? = True or nest? = True [set size 1 + (food / (food_capacity * food_location)) * 5]
      
    ]
    ask ants[ifelse food_transported > 0[set color yellow set size 2][set color red set size 1.5]]
    ; ask links[;;ifelse pheromones > 0[set color white][set color blue]
    ;if pheromones < 0.0000001[set pheromones 0.0000001 set color white ]
    ; if pheromones > 0.4[set color blue - 2]
    ;if pheromones > 1[set color blue]
    ;if pheromones > 3[set color blue + 3]
    ;]
    tick
  ]
end
@#$#@#$#@
GRAPHICS-WINDOW
210
10
649
470
16
16
13.0
1
10
1
1
1
0
1
1
1
-16
16
-16
16
1
1
1
ticks
30.0

BUTTON
28
156
91
189
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
22
23
194
56
ant_population
ant_population
0
100
50
1
1
NIL
HORIZONTAL

SLIDER
28
114
200
147
total_locations
total_locations
4
40
25
1
1
NIL
HORIZONTAL

BUTTON
125
158
188
191
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
13
358
185
391
evaporate_rate
evaporate_rate
0
1
0.22
0.01
1
NIL
HORIZONTAL

PLOT
712
98
1081
288
food in nest
tick
food
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"link" 1.0 0 -14439633 true "" "if smart_ant? = True [let o 0\nask locations[ if nest? = True [set o food plot o]]\n]"
"pen-1" 1.0 0 -2674135 true "" "if smart_ant? = False[let o 0\nask locations[ if nest? = True [set o food plot o]]\n]"

SLIDER
15
255
187
288
alpha
alpha
0
5
0.64
0.01
1
NIL
HORIZONTAL

SLIDER
25
311
197
344
beta
beta
0
5
0.51
0.01
1
NIL
HORIZONTAL

SLIDER
757
30
929
63
food_capacity
food_capacity
0
10000
4076
1
1
NIL
HORIZONTAL

BUTTON
57
430
120
463
NIL
reset
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
698
366
880
399
nmb_location_connection
nmb_location_connection
0
50
5
1
1
NIL
HORIZONTAL

SLIDER
12
394
184
427
ant_food_capacity
ant_food_capacity
0
100
40
1
1
NIL
HORIZONTAL

SLIDER
698
301
870
334
food_location
food_location
1
10
3
1
1
NIL
HORIZONTAL

BUTTON
77
198
140
231
NIL
go
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
25
71
197
104
smart_ant_population
smart_ant_population
0
100
9
1
1
NIL
HORIZONTAL

SWITCH
717
426
835
459
smart_ant?
smart_ant?
1
1
-1000

@#$#@#$#@
## WHAT IS IT?

(a general understanding of what the model is trying to show or explain)

## HOW IT WORKS

(what rules the agents use to create the overall behavior of the model)

## HOW TO USE IT

(how to use the model, including a description of each of the items in the Interface tab)

## THINGS TO NOTICE

(suggested things for the user to notice while running the model)

## THINGS TO TRY

(suggested things for the user to try to do (move sliders, switches, etc.) with the model)

## EXTENDING THE MODEL

(suggested things to add or change in the Code tab to make the model more complicated, detailed, accurate, etc.)

## NETLOGO FEATURES

(interesting or unusual features of NetLogo that the model uses, particularly in the Code tab; or where workarounds were needed for missing features)

## RELATED MODELS

(models in the NetLogo Models Library and elsewhere which are of related interest)

## CREDITS AND REFERENCES

(a reference to the model's URL on the web if it has one, as well as any other necessary credits, citations, and links)
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270

@#$#@#$#@
NetLogo 5.1.0
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180

@#$#@#$#@
0
@#$#@#$#@
